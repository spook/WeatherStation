#include "Radio.h"
#include "Battery.h"
#include "DHT.h"

byte rootAddress[] = {0x00, 0x00, 0x01};
byte sensorAddress[] = {0x00, 0x00, 0x02};

// DHT-22 on pin 8
DHT dht(8, 22);

struct Data {
  int temp;
  int humid;
  int batt;
};

void setup() {
  // Starting up DHT
  dht.begin();
  // Starting up radio
  Radio.begin(sensorAddress, 100);
}

void loop() {
  // Give DHT time to warm up
  delay(3000);

  // Make sure, that read is correct

  Data data;
  
  do 
  {
    data.temp = (int)(dht.readTemperature() * 100);
    data.humid = (int)(dht.readHumidity() * 100);
  }
  while (data.temp == 0 || data.humid == 0);

  // Read battery status
  data.batt = batteryRead();

  Radio.write(rootAddress, (unsigned char *)&data, sizeof(data));
  Radio.flush();

  Radio.off();
  sleep(30000);
}
