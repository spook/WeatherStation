#include <Adafruit_BME280.h>
#include <DS3231.h>
#include <LiquidCrystal_PCF8574.h>
#include "RadioNRF24.h"

struct Data {

  int temp;
  int humidity;
  int battery;
};

LiquidCrystal_PCF8574 lcd(0x27); // Zielony 0x3f; Niebieski 0x27
Adafruit_BME280 bmp;
DS3231 rtc;
Data data;

byte address[] = {0x00, 0x00, 0x01};

char * line = new char[32];

void setup()
{
  // For debugging
  // Serial.begin(9600);
  // while (!Serial) ;
  
  lcd.begin(20, 4);
  lcd.clear();
  lcd.setBacklight(255);

  byte degree[8] = {
    B01110,
    B10001,
    B10001,
    B01110,
    B00000,
    B00000,
    B00000,
    B00000
  };
  lcd.createChar(1, degree);

  byte temp[8] = {
    B00100,
    B00110,
    B00100,
    B00110,
    B00100,
    B01110,
    B01110,
    B01110
  };
  lcd.createChar(2, temp);

  byte humid[8] = {
    B00000,
    B00100,
    B00100,
    B01110,
    B01110,
    B11111,
    B11111,
    B01110
  };
  lcd.createChar(3, humid);

  byte pressure[8] = {
    B11111,
    B01110,
    B00100,
    B00000,
    B00000,
    B00100,
    B01110,
    B11111
  };
  lcd.createChar(4, pressure);
  
  
  RadioNRF24.begin(address, 100, 5, 4);

  bmp.begin(0x76);

  // Let BME280 init
  delay(500);

  data.temp = 0;
  data.humidity = 0;
  data.battery = 0;
} 

void printTemperature() {

  memset(line, ' ', 20);
  line[20] = 0;
  
  int temp = (int)(bmp.readTemperature() * 10);
  snprintf(line, 21, "\02 %2d.%d\01C     %2d.%d\01C ", temp / 10, temp % 10, data.temp / 100, data.temp / 10 % 10);

  lcd.setCursor(0, 0);
  lcd.print(line);
}

void printHumidity() {

  memset(line, ' ', 20);
  line[20] = 0;
  
  int humidity = (int)(bmp.readHumidity() * 10);
  snprintf(line, 21, "\03 %2d.%d%%      %2d.%d%%   ", humidity / 10, humidity % 10, data.humidity / 100, data.humidity / 10 % 10);
  
  lcd.setCursor(0, 1);
  lcd.print(line);
}

void printPressure() {

  memset(line, ' ', 20);
  line[20] = 0;

  long pressure = bmp.readPressure();
  snprintf(line, 21, "\04 %4d.%d hPa  %d.%2dV", (int)(pressure / 100), (int)(pressure / 10 % 10), data.battery / 1000, data.battery / 10 % 100);
  
  lcd.setCursor(0, 2);
  lcd.print(line);  
}

void printTime() {

  bool h12, pm;
  int hour = rtc.getHour(h12, pm);
  int minute = rtc.getMinute();
  int second = rtc.getSecond();

  memset(line, ' ', 20);
  line[20] = 0;

  snprintf(line, 20, "     %02d:%02d:%02d", hour, minute, second);
  lcd.setCursor(0, 3);
  lcd.print(line);
}

void loop()
{
  if (RadioNRF24.available() > 0) {

    RadioNRF24.read((unsigned char *)&data);

    // For debugging
    // char text[32];
    // snprintf(text, 32, "T: %d, H: %d, B:%d", data.temp, data.humidity, data.battery);
    // Serial.println(text);
  }
  
  printTemperature();
  printHumidity();
  printPressure();
  printTime();

  delay(500);

  printTime();

  delay(500);
} 
